Hello, I am Mario Hoffmann, Software-Engineer by profession with a big heart for C/C++ and VHDL.
My main focus is on embedded systems and FPGA-Design.

## Favorite Languages

- C/C++
- VHDL
- Python

## Projects
- [mrv32](https://gitlab.com/hoffma/mrv32) - New RISC-V CPU, trying to work towards higher max_f, have cleaner code and possibly make it pipelined. Currently able to run the RV32i instrunction set.
    - [mrv32-SoC](https://gitlab.com/hoffma/mrv32-soc) - A very basic System-On-Chip to test the CPU on a real Hardware (ECP5 for now).

### Past projects
- [miniRV32](https://gitlab.com/hoffma/minirv32) - Small RISC-V CPU implementing the RV32i instruction set. My very first RISC-V testbench.


## Masterthesis

- "Entwurf und Implementierung eines Cache- und SDRAM-Controllers für eine ARMv4 Mikroarchitektur auf Xilinx Kintex FPGAs"
    - [ARM SDRAM Core](https://gitlab.com/hoffma/arm-sdram-core)
    - The thesis can be found here: https://gitlab.com/hoffma/arm-sdram-core/-/blob/master/masterarbeit_hoffmann_555625.pdf (Hint: There is a typo on the cover. It was handed in 2022, not 2021).
    
The goal of this thesis was to design a Cache- and SDRAM-Controller for an ARMv4 microprocessor which was designed by another student.



